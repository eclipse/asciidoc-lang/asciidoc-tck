#!/usr/bin/env node

import process from 'node:process'
import ospath from 'node:path'
import fsp from 'node:fs/promises'
import { once } from 'node:events'

process.title = 'Echo AsciiDoc TCK Adapter'

/**
 * @returns {Promise<string>}
 */
const readFromStdin = async () =>
  new Promise((resolve, reject) => {
    const encoding = 'utf-8'
    let data
    data = ''
    process.stdin.setEncoding(encoding)
    process.stdin.on('readable', function () {
      const chunk = process.stdin.read()
      if (chunk !== null) {
        data += chunk
      }
    })
    process.stdin.on('error', (error) => {
      reject(error)
    })
    process.stdin.on('end', function () {
      // There will be a trailing \n from the user hitting enter. Get rid of it.
      data = data.replace(/\n$/, '')
      resolve(data)
    })
  })

;(async () => {
  const content = await readFromStdin()
  const payload = JSON.parse(content)
  const outputFile = ospath.join(payload.path.replace(/-input\.adoc$/, '-output.json'))
  const expected = await fsp.readFile(outputFile, 'utf8')
  let result = expected
  if (payload.type === 'inline') {
    const inlines = JSON.parse(expected)
    result = JSON.stringify({
      name: 'document',
      type: 'block',
      blocks: [
        {
          name: 'paragraph',
          type: 'block',
          inlines,
        },
      ],
    })
  }
  process.stdout.write(result)
  await once(process.stdout.end(), 'close')
})()
