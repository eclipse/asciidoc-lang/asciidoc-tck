import { describe, it } from 'node:test'
import assert from 'node:assert'
import ospath from 'node:path'
import runner from '#runner'
import { toModuleContext } from '../lib/helpers.js'

const ECHO_ADAPTER_DIR = ospath.join(toModuleContext(import.meta).__dirname, 'sample-adapters/echo')

describe('runner()', () => {
  // QUESTION: should we count "*-output.json" files in the tests directory
  const testsCount = 13

  it('should fail when using an non existing command adapter', async () => {
    const testsResult = await runner({
      adapter: {
        cli: {
          command: 'non-existing-command-adapter',
        },
      },
      reporter: false,
    })
    assert.strictEqual(testsResult.fail, testsCount)
  })
  it('should fail when using an non existing server adapter', async () => {
    const testsResult = await runner({
      adapter: {
        server: {
          url: 'http://noop',
        },
      },
      reporter: false,
    })
    assert.strictEqual(testsResult.fail, testsCount)
  })
  it('should succeed when using the echo adapter', async () => {
    const testsResult = await runner({
      adapter: {
        cli: {
          command: `npx node ${ECHO_ADAPTER_DIR}/index.js`,
        },
      },
      reporter: false,
    })
    assert.deepStrictEqual(testsResult.errors, [])
    assert.strictEqual(testsResult.fail, 0)
    assert.strictEqual(testsResult.pass, testsCount)
  })
})
