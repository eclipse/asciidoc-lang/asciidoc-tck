import fsp from 'node:fs/promises'
import process from 'node:process'
import ospath from 'node:path'

export default async function loadTests (dir = process.cwd(), base = process.cwd()) {
  const entries = []
  if (!ospath.isAbsolute(dir)) dir = ospath.resolve(dir)
  for await (const dirent of await fsp.opendir(dir)) {
    const name = dirent.name
    if (dirent.isDirectory()) {
      const childEntries = await loadTests(ospath.join(dir, name), base)
      if (childEntries.length) entries.push({ type: 'dir', name, entries: childEntries })
    } else if (name.endsWith('-input.adoc')) {
      const basename = name.slice(0, name.length - 11)
      const inputPath = ospath.join(dir, name)
      const outputPath = ospath.join(dir, basename + '-output.json')
      const configPath = ospath.join(dir, basename + '-config.json')
      entries.push(
        await Promise.all([
          fsp.readFile(inputPath, 'utf8'),
          fsp.readFile(outputPath).then(
            (data) => [JSON.parse(data), JSON.parse(data, (key, val) => (key === 'location' ? undefined : val))],
            () => []
          ),
          fsp
            .readFile(configPath)
            .then((data) => JSON.parse(data))
            .catch(() => ({})),
        ]).then(([input, [expected, expectedWithoutLocations], config]) => {
          if (config.trimTrailingWhitespace) {
            input = input.trimEnd()
          } else if (config.ensureTrailingNewline) {
            if (input[input.length - 1] !== '\n') input += '\n'
          } else if (input[input.length - 1] === '\n') {
            input = input.slice(0, input.length - 1)
          }
          return {
            type: 'test',
            name: config.name || basename.replace(/-/g, ' '),
            condition: config.only ? 'only' : config.skip ? 'skip' : undefined,
            data: {
              basename,
              inputPath: ospath.relative(base, inputPath),
              outputPath: ospath.relative(base, outputPath),
              input,
              expected,
              expectedWithoutLocations,
            },
          }
        })
      )
    }
  }
  return entries.sort((a, b) => {
    if (a.type !== b.type) return a.type === 'test' ? -1 : 1
    return a.name.localeCompare(b.name)
  })
}
