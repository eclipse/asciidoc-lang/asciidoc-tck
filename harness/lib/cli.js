import { parseArgs } from 'node:util'
import process from 'node:process'

export default function resolveCliArgs (args = process.argv.slice(2)) {
  const { values, positionals } = parseArgs({
    args,
    allowPositionals: true,
    options: {
      'start-command': {
        type: 'string',
      },
      'stop-command': {
        type: 'string',
      },
      url: {
        type: 'string',
        short: 'u',
      },
      'adapter-command': {
        type: 'string',
        short: 'c',
      },
      tests: {
        type: 'string',
      },
    },
  })
  if (positionals.length === 0) {
    throw new Error('You must specify the TCK adapter mode using either "cli" or "server" as a positional argument')
  }
  const testsDir = values.tests
  const config = testsDir ? { testsDir } : {}
  const startCommand = values['start-command']
  const stopCommand = values['stop-command']
  if (positionals.length === 1) {
    const adapterMode = positionals[0]
    if (adapterMode === 'cli') {
      if (values['adapter-command'] === undefined) {
        throw new Error('You must specify the TCK adapter command using --adapter-command or -c')
      }
      return {
        ...config,
        adapter: {
          cli: {
            command: values['adapter-command'],
          },
        },
      }
    }
    if (adapterMode === 'server') {
      const url = values.url
      if (url === undefined && startCommand === undefined) {
        throw new Error(
          'You must specify either specify a start command using --start-command or an URL using --url or -u'
        )
      }
      return {
        ...config,
        adapter: {
          server: {
            ...(url && { url }),
            ...(startCommand && { startCommand }),
            ...(stopCommand && { stopCommand }),
          },
        },
      }
    }
    // assumes that type is CLI and that the positional argument is the command
    return {
      ...config,
      adapter: {
        cli: {
          command: positionals[0],
        },
      },
    }
  }
  if (positionals.length === 2) {
    const adapterMode = positionals[0]
    if (adapterMode === 'cli') {
      return {
        ...config,
        adapter: {
          cli: {
            command: positionals[1],
          },
        },
      }
    }
    if (adapterMode === 'server') {
      return {
        ...config,
        adapter: {
          server: {
            url: positionals[1],
            ...(startCommand && { startCommand }),
            ...(stopCommand && { stopCommand }),
          },
        },
      }
    }
    throw new Error('The TCK adapter mode must be either "cli" or "server"')
  }
}
