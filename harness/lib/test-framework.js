import { describe, it } from 'node:test'

export function makeTests (testCases, testBlock) {
  for (const testCase of testCases) {
    const { name, type } = testCase
    if (type === 'dir') {
      describe(name, () => makeTests(testCase.entries, testBlock))
    } else {
      ;(it[testCase.condition] || it)(name, function () {
        return testBlock.call(this, testCase.data)
      })
    }
  }
}

export function populateASGDefaults (node) {
  if (node.type !== 'block') return node
  const metadata = node.metadata
  if (metadata) {
    metadata.attributes ??= {}
    metadata.options ??= []
  }
  if (node.form === 'macro' || node.name === 'break' || node.name === 'heading') return node
  if (['listing', 'literal', 'pass', 'stem', 'paragraph', 'verse'].includes(node.name)) {
    node.inlines ??= []
  } else if (['list', 'dlist'].includes(node.name)) {
    node.items.forEach(populateASGDefaults)
  } else {
    ;(node.blocks ??= []).forEach(populateASGDefaults)
  }
  return node
}

function stripASGDefaults (node) {
  if (node.type !== 'block') return node
  const metadata = node.metadata
  if (metadata) {
    if (!Object.keys(metadata.attributes).length) delete metadata.attributes
    if (!metadata.options.length) delete metadata.options
  }
  if (node.form === 'macro' || node.name === 'break' || node.name === 'heading') return node
  if (['listing', 'literal', 'pass', 'stem', 'paragraph', 'verse'].includes(node.name)) {
    if (!node.inlines.length) delete node.inlines
  } else if (['list', 'dlist'].includes(node.name)) {
    node.items.forEach(stripASGDefaults)
  } else if (node.blocks?.length) {
    node.blocks.forEach(stripASGDefaults)
  } else {
    delete node.blocks
  }
  return node
}

export function stringifyASG (asg) {
  const locations = []
  return JSON.stringify(
    stripASGDefaults(asg),
    (key, val) => (key === 'location' ? locations.push(val) - 1 : val),
    2
  ).replace(/("location": )(\d+)/g, (_, key, idx) => {
    return (
      key +
      JSON.stringify(locations[Number(idx)], null, 2)
        .replace(/\n */g, ' ')
        .replace(/(\[) | (\])/g, '$1$2')
    )
  })
}

export * from 'node:test'
export * as assert from 'node:assert'
