export default async () => {
  return {
    input: 'harness/lib/index.js',
    output: {
      intro: 'globalThis.NODE_SEA = "true";',
      file: 'dist/index.cjs',
      format: 'cjs'
    },
    external: [
      'node:process',
      'node:path',
      'node:util',
      'node:test',
      'node:assert',
      'node:url',
      'node:child_process',
      'node:fs/promises',
      'node:test/reporters'
    ]
  }
}
